###########################
# Running the PM workflow #
###########################

#
#
#
#
#


source("Decomposition_Functions.R")
# this file can be found at 
# www.helsinki.fi/bsg/software/Biolog_Decomposition/Decomposition_Functions.R

source("PMworkflow.R") 
# This file can be found at 
# www.helsinki.fi/bsg/software/R-Biolog/
# It requires "PMvis.R" which can be found at the same place

library(opm)




################
# Data Loading #
################


# Load the example data 
	EColi.opm <- read_opm(paste("data",(list.files("data")),sep="/"))
	str(EColi.opm)
# this file can be found at www.helsinki.fi/bsg/software/Biolog_Decomposition/Example_data.7z



# Define metadata
	metadata(EColi.opm) <- to_metadata(csv_data(EColi.opm))[,c("Plate Type","Strain Number")]
	str(EColi.opm)


# Define additional metadata; temperature and replicate id's
	metadata.EColi <- collect_template(paste("data",(list.files("data")),sep="/"))
	metadata.EColi$Replicate <- rep(c("rep1","rep2","rep3"),3)
	metadata.EColi$Strain <- rep(c("IMT17887","PCV17887","T17887"),each=3)
	EColi.opm <- include_metadata(EColi.opm, md = metadata.EColi)


# Convert the S4 object into a data frame compatible with our functions
	EColi <- S4toList(EColi.opm, add.variables = c("Strain", "Replicate"))

# Use PM.Summary -function to see a summary of the data
	PM.Short.Summary(EColi)
	PM.Summary(EColi)


# Subtract the negative control
EColi_corrected <- PM.BgCorrection(EColi,control='H04')


# Plot a sample plate
PM.plot.data(EColi_corrected$data[[1]],signalname='BgCorrected')



########################################
# EXAMPLE oF BIOLOG DECOMPOSITION:     #
# Decomposing a single signal          #
########################################

# as an example, we will use strain IMT17887, replicate 1, substrate D10 (Lactulose)

frame = EColi_corrected$data[[1]]$data_frame

D = PM.get.grow(frame,signalname='BgCorrected',d=0.5)
signal = D$Growth[ D$Substrate=='D10' ]
time   = D$Time[ D$Substrate=='D10' ]

# raw signal
plot(time,D$BgCorrected[ D$Substrate=='D10' ],t='l')

# target signal
plot(time,signal,t='l')
plot(time,cumsum(signal),t='l')


# how the decomposition looks like
# different parameter settings may produce a different results
decompose = MS.decompose(signal,time,threshold=20,smooth=0.5,corrcoef=2)

# list of all components
decompose

# number of components
decompose$pow

# first component
decompose[[1]]

# type of the first component and its parameters
decompose[[1]]$model
decompose[[1]]$par

########################################
# EXAMPLE oF BIOLOG DECOMPOSITION:     #
# Decompose thw whole plate            #
########################################



decomposition = MS.frame.decompose(EColi_corrected$data[[1]],smooth=0.5,corrcoef=2,threshold=20, signalname = 'Signal')

# output value includeds both the original data and all the decompositions
str(decomposition$Decomposition)
decomposition$Decomposition$A11

# new raw "Growth" is added to the data_frame
str(decomposition$data_frame)

# visualizing the results
MS.plot.decomposition(decomposition)


# the following code decomposes the whole dataset
# (would take some time) #
for (ii in 1:9){
  decomposition = MS.frame.decompose(EColi_corrected$data[[ii]],smooth=0.5,corrcoef=2,threshold=20, signalname = 'Signal')
  
  png(paste("visH_",ii,".png",sep=''), height = 9.19, width = 6.8, units = 'in', res=300)
  MS.plot.decomposition(decomposition,t='overlap', signalname = 'Signal')
  dev.off()
}



########################################
# EXAMPLE oF BIOLOG DECOMPOSITION:     #
# Decomposing step by step             #
########################################
# the following code illustrates how the decomposition method works.
# function MS.decompose() conducts these oprations automatically


# as an example, we will use strain IMT17887, replicate 1, substrate D10 (Lactulose)
frame = EColi_corrected$data[[1]]$data_frame

D = PM.get.grow(frame,signalname='BgCorrected',d=0.5)
signal = D$Growth[ D$Substrate=='D10' ]
time   = D$Time[ D$Substrate=='D10' ]


# how the first candidats for the components looks like
plot(time,signal,t='l')
C1 = fit.gauss(signal, time, 0.5)
C2 = fit.brick(signal, time, 0.5)
C3 = fit.slope(signal, time, 0.5)
draw.decomposition(list(C1,C2,C3,pow=3),time) 

# what component fit the best?
C1$err
C2$err
C3$err
#C1 is the best (have the smallest error)

# how the second candidates for the components looks like
existing_components=C1$pred
plot(time,signal-existing_components,t='l')
C4 = fit.gauss(signal-existing_components, time, 0.5)
C5 = fit.brick(signal-existing_components, time, 0.5)
C6 = fit.slope(signal-existing_components, time, 0.5)
draw.decomposition(list(C4,C5,C6,pow=3),time) 

C4$err
C5$err
C6$err


# how the third candidates for the components looks like
existing_components = C1$pred + C4$pred
plot(time,signal-existing_components,t='l')
C7 = fit.gauss(signal-existing_components, time, 0.5)
C8 = fit.brick(signal-existing_components, time, 0.5)
C9 = fit.slope(signal-existing_components, time, 0.5)
draw.decomposition(list(C7,C8,C9,pow=3),time) 

C7$err
C8$err
C9$err

# how the fourth candidates for the components looks like
existing_components = C1$pred + C4$pred + C9$pred
plot(time,signal-existing_components,t='l')
C10 = fit.gauss(signal-existing_components, time, 0.5)
C11 = fit.brick(signal-existing_components, time, 0.5)
C12 = fit.slope(signal-existing_components, time, 0.5)
draw.decomposition(list(C7,C8,C9,pow=3),time) 

C10$err
C11$err
C12$err

# the best candidtate is too small
sum(C11$pred)


# starting the calibration
# calibration, pt 1
# we end up having three components: C1, C4 and C9
# re-fitting of the first component
fixed_components =  C4$pred + C9$pred
plot(time,     signal-fixed_components, t='l')
C10 = fit.gauss(signal-fixed_components, time, 0.5, fixed_components)
C11 = fit.brick(signal-fixed_components, time, 0.5, fixed_components)
C12 = fit.slope(signal-fixed_components, time, 0.5, fixed_components)
draw.decomposition(list(C10,C11,C12,pow=3),time) 

C10$err
C11$err
C12$err

# re-fitting the second component
fixed_components =  C10$pred + C9$pred
plot(time,     signal-fixed_components, t='l')
C13 = fit.gauss(signal-fixed_components, time, 0.5, fixed_components)
C14 = fit.brick(signal-fixed_components, time, 0.5, fixed_components)
C15 = fit.slope(signal-fixed_components, time, 0.5, fixed_components)
draw.decomposition(list(C13,C14,C15,pow=3),time) 

# and so on




########################################
# EXAMPLE oF BIOLOG DECOMPOSITION:     #
# comparison of the decompositions     #
########################################

# as an example, we will use substrate A08 (L-Proline)
decompose = list()
substr = 'A08'


for (i in 1:9){
  frame = EColi_corrected$data[[i]]$data_frame
  D = PM.get.grow(frame,signalname='Signal',d=0.5)
  decompose[[i]] = MS.decompose(D$Growth[ D$Substrate==substr ],time,smooth=0.5,corrcoef=2,threshold=20)
}


# draw decompositions

par(mfrow=c(3,3))
  
for (i in 1:9){
    plot(c(min(time),max(time)),c(0,10),t='n',bty='L',xlab='Time',ylab='Growth')
    frame = EColi_corrected$data[[i]]$data_frame
    lines(time,frame$Signal[frame$Substrate==substr]*10/400,col=1)

    draw.decomposition(decompose[[i]],time,lwd=3,t='overlap') 
}





# proposed distance

M1 = matrix(1,9,9)
for (i in 1:8){
  for (j in (i+1):9){
    M1[i,j]=MS.similarity(decompose[[i]],decompose[[j]], d_max=3, d_size=100, d_center=30)
    M1[j,i]=M1[i,j]
}}


# Euclidian distance

M2 = matrix(1,9,9)
for (i in 1:8){
  for (j in (i+1):9){
    frame = EColi_corrected$data[[i]]$data_frame
    L1 = frame$Signal[frame$Substrate==substr]

    frame = EColi_corrected$data[[j]]$data_frame
    L2 = frame$Signal[frame$Substrate==substr]

    M2[i,j]=-sum((L1-L2)^2)
    M2[j,i]=M2[i,j]
  }}


# draw matrixes

par(mfrow=c(1,2))
image(M2,col = gray(0:100/100),main='Euclidian distance')
image(M1,col = gray(0:100/100),main='decomposition similarity')
