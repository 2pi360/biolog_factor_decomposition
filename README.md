BIOLOG_factor_decomposition
---------------------------

This is code for 2015 paper

Novel R Pipeline for Analyzing Biolog Phenotypic Microarray Data

https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0118392



and  for the 2016 paper

Identifying Multiple Potential Metabolic Cycles in Time-Series from Biolog Experiments

https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0162276
