R-Biolog folder contains: 

1) PMworkflow
This R source file includes R functions needed for executing the R-Biolog pipeline. The functions can be loaded into R workspace by using e.g. command source("PMworkflow.R"). An example of loading and running the functions is given in file runPM.

2) PMvis
This R source file includes R visualization functions. The functions can be loaded into R workspace by using e.g. command source("PMvis.R"). An example of loading and running the functions is given in file runPM.

3) runPM
This R file includes r commands for running the R-Biolog pipeline. More detailed information e.g. on adjusting the arguments for the R functions are given in the user manual.
 
4) User manual
This file guides through running the R-Biolog pipeline. For example, information about the input and output of the r functions, as well as about adjusting arguments, are provided.

5) exampleData
This folder contains 12 csv files, each including longitudinal measurements of metabolic signals on a single Biolog microplate.

6) comparison
This file is a variance analysis model needed for testing effects with WinBUGS program.



The algorithms and data set are described in Vehkala, M., Shubin, M, Connor, T.R., Thomson, N.R. and Corander, J. (2014) Novel R pipeline for analyzing Biolog phenotypic microarray data (submitted).

To complete the R-Biolog pipeline WinBUGS software (www.mrc-bsu.cam.ac.uk/software/bugs/the-bugs-project-winbugs) is needed.

If you have any questions regarding this software, please contact minna.vehkala@helsinki.fi.


(updated October 1, 2014)



