##
## Set of functions for the BIOLOG signal decomposition
##


#################################################
### Auxiliary functions for working with data ###
#################################################


## Signal smoothing with Gaussian Kernel
##    Input:
## L - signal
## T - time point
## d - kernel variance (parameter b)

PM.smooth.Signal <- function(L,T,d = 1) {
  if (d>0){
    n = length(L)
    S = rep(0,n)
    E = rep(0,n)
    C = dnorm(T,T[1],d)
    for (i in 2:(n-1)) {
      K = c(C[i:2], C[1:(n-i+1)])
      S = S+L[i]*K
      E = E+K
    }
    S = S+L[1]*C
    E = E+C
    
    S = S+L[n]*C[n:1]
    E = E+C[n:1]
    
    S/E
  }
  else
  {L}
}



## Smoothing all signals in a single frame with Gaussian Kernel
##    Input:
## frame - frame 
## signalname - name of the field there the signal is contained
## d - kernel variance (parameter b)

PM.smooth.frame <- function(frame, signalname, d = 1) {
  Substrates = unique(frame$Substrate)
  for (s in Substrates){
    L = frame[[signalname]][frame$Substrate==s]
    T = frame$Time[frame$Substrate==s]
    frame[[signalname]][frame$Substrate==s] = PM.smooth.Signal(L,T,d)
  }
  frame
}


## Performs the Preprocessing: extracts the smoothed lagged difference from the singal
##    Input:
## L - signal
## T - time point
## d - smoothing kernel variance (parameter b)


PM.get.grow <- function(frame,signalname,d = 1) {
  Substrates = unique(frame$Substrate)
  frame$Growth=0
  for (s in Substrates){
    L = frame[[signalname]][frame$Substrate==s]
    T = frame$Time[frame$Substrate==s]
    S = c(0,L[-1]-L[-length(L)])
    
    #if (sum(S)>0){
    #  S = S/sum(S)*L[length(L)]
    #}
    growth = PM.smooth.Signal(S,T,d)
    frame$Growth[frame$Substrate==s] =growth
  }

  frame
}


# EDITED SINCE
PM.get.grow2 <- function(frame,signalname,d = 1) {
  Substrates = unique(frame$Substrate)
  frame$Growth=0
  for (s in Substrates){
    L = frame[[signalname]][frame$Substrate==s]
    T = frame$Time[frame$Substrate==s]
    S = c(0,L[-1]-L[-length(L)])
    S = PM.smooth.Signal(S,T,d)
    S[S<0]=0
    if (sum(S)>0){
      S = S/sum(S)*L[length(L)]
    }
    #growth = PM.smooth.Signal(S,T,d)
    frame$Growth[frame$Substrate==s] =S
  }
  
  frame
}



#####################
### DECOMPOSITION ###
#####################




## optmization function
##     Input:
## signal - signal
## time - time point
## fun - component generating function
## A1, B1 - minimal and maximal bounds for the first optimized parameter
## second_par_is_var - boolean, true if the second optimized parameter represent the variance. This argument affects the bounds of the optimization.
## positions - sum of all other component. Used during the calibration step. NaN during the initial decomposition step.
## corrcoef - coefficient, scaling the effect of correlation (parameter gamma)

MS.optimize  <- function(signal,
                         time,
                         fun, 
                         A1=0,
                         B1=max(time),
                         first_par_is_int=FALSE,
                         second_par_is_var=TRUE,
                         positions=NaN,
                         corrcoef=2,
                         d=0.5){

  distanceFun  <- function(x, fixed, signal, time, positions, d){
    pred = fun(fixed, x, time)
    pred = PM.smooth.Signal(pred, time, d)
    
    if (sum(pred)>0 & is.finite(sum(pred)))  {
      A = sum(pred*signal)/sum(pred^2) 
      A = max(A,0)
      pred = A * pred
    } else {
      pred=time*0
      A=0
    }

    if (  any(is.nan(positions)) | (is.null(positions))  ){
      sum((signal - pred)^2)
    }else{
      sum((signal - pred)^2)  + sum(pred*positions)*corrcoef
    }
    
  }
  
  h = (B1-A1)/10
  X_list = (0:10)*h+A1
  if (first_par_is_int & (h<0.25)) {
    X_list = seq(A1,B1,0.25)
    h=0
  }
  
  vvv = c(Inf,A1,0)
  
  for (x in X_list){
    if (second_par_is_var){ 
      bounds = c(1,100)
    } else { 
      bounds = c(x+0.25, max(time)+0.5)
    }
    result=optimize(   distanceFun, 
                       bounds,
                       signal = signal, 
                       positions = positions,
                       time = time,
                       fixed = x,
                       d=d,
                       tol=max(0.1,h)/10
                       )
    
    if (result$objective<vvv[1]){
      vvv = c(result$objective, x, result$minimum)
    } 
  }
  

  if (h<0.1) {
    pred = fun(vvv[2], vvv[3], time)
    pred = PM.smooth.Signal(pred, time, d)
    
    A = sum(pred*signal)/sum(pred^2)
    pred  = A * pred

    
    list(err=vvv[1],A=A,par1=vvv[2],par2=vvv[3],pred=pred)
  }else{
    MS.optimize(signal,
                time,
                fun,
                max(A1,vvv[2]-h),
                min(B1,vvv[2]+h),
                first_par_is_int,
                second_par_is_var,
                positions,
                corrcoef=corrcoef,
                d=d)
  }
}


# These are function to fit a model to data.  
# Arguments:
#             signal, time: vectors with the data
# Output:
#             list with four objects 
#   0) model type ('gauss','brieck', etc.)
#   1) estimated parameters
#   2) predicted curve
#   3) model fit (measured in the sum of squre errors), 


gauss.curve <- function (time,mean,var) exp(-(mean - time)^2/(2*var))

fit.gauss <- function(signal,
                      time,
                      d,
                      positions=c(),
                      corrcoef=2) 
{
  getPred    <- function(mean, var, xx) {
    pred = gauss.curve(xx,mean,var)
  }

  result = MS.optimize(signal,
                       time,
                       getPred, 
                       first_par_is_int=FALSE, 
                       second_par_is_var=TRUE, 
                       positions=positions,
                       corrcoef=corrcoef,
                       d=d)

  par=c(A=result$A, mid=result$par1, var=result$par2)
  pred = result$pred
  list(model = 'gauss', par = par, error = result$err, pred=pred, weight=sum(pred))
} 



fit.slope <- function(signal,
                      time,
                      d=d,
                      positions=c(),
                      corrcoef=2) 
{
  getPred    <- function(t0,tm,xx) {
    pred =  1 / ((tm-t0-t0 + xx)^2)  
    pred[xx<t0]= 0
    pred
  }
  
  result = MS.optimize(signal,
                       time,
                       getPred,  
                       first_par_is_int=TRUE, 
                       second_par_is_var=FALSE, 
                       positions=positions,
                       corrcoef=corrcoef,
                       d=d)
  
  par=c(A=result$A, t0=result$par1, tm=result$par2)
  pred = result$pred
  list(model = 'slope', par = par, error = result$err, pred=pred, weight=sum(pred))
} 



fit.brick <- function(signal,
                      time,
                      d,
                      positions=c(),
                      corrcoef=2) 
{
  getPred    <- function(t0,t1, xx) {
    pred =  rep(1,length(xx))
    pred[xx<t0]=0
    pred[xx>t1]=0
    pred
  }
  
  result = MS.optimize(signal,
                       time,
                       getPred,  
                       first_par_is_int=TRUE, 
                       second_par_is_var=FALSE, 
                       positions=positions, 
                       corrcoef=corrcoef, 
                       d=d)
  
  par=c(A=result$A, t0=result$par1, t1=result$par2)
  pred = result$pred
  list(model = 'brick', par = par, error = result$err, pred=pred, weight=sum(pred))
} 





## optmization function, fits the best component
##    Input:
## signal - signal
## time - time point
## positions - sum of all other component. Used during the calibration step. NaN during the initial decomposition step.
## corrcoef - coefficient, scaling the effect of correlation (parameter gamma)
##    Output: component

fit.component <- function(signal,time,positions=c(),corrcoef=2,smooth=0.5) {
  BG = fit.gauss(signal,time,smooth,positions,corrcoef)
  BB = fit.slope(signal,time,smooth,positions,corrcoef)
  BS = fit.brick(signal,time,smooth,positions,corrcoef)
  errG = BG$error
  errB = BB$error
  errS = BS$error
  
  if ((errG<errB) & (errG<errS)) {BG} else {
    if ((errB<errG) & (errB<errS)) {BB} 
    else {BS} }
}



## decomposition function, decomposes a single pre-processed signal
##    Input:
## signal - signal
## time - time point
## threshold - threshold parameter
## show - if TRUE, plots the decomposition process
##    Output: list
## 1--n - list of n components
## pow - power of decomposition, i.e. the number of components n

MS.decompose <- function(signal, time, smooth, threshold = 15, corrcoef=2, show = TRUE) { 
  n=length(signal)
  R = signal # residuals
  Components=list()
  pow=0
  if (sum(R)>0){
    C = fit.component(R, time, signal-R,0, smooth)
    while (C$weight>threshold) {
      pow=pow+1
      Components[[pow]] = C
      R = R - C$pred

      if (show){
        plot(time,signal,t='l',ylim=c(-2,7),main='initial',col=gray(0.5),bty = "L")
        polygon(c(time[1],time,time[length(time)]),c(0,signal,0),col=gray(0.8),border=NA)
        #lines(time,R)
        draw.decomposition(Components,time,pow=pow)
      }
      
      C = fit.component(R, time, signal-R,0, smooth)
    
    }
  }
  

  if (pow>1){
    err = sum(R^2)
    err0 = Inf
    t=0
    while (( (err0-err)>0.001) & (t<10) ) { # stopping condition

      t=t+1
      j=0
      new_Components=list()
      
      for (i in 1:pow) {
        C = Components[[i]]
        R = R+C$pred
        C = fit.component(R, time, signal-R, corrcoef, smooth)
        if (C$weight>threshold) {
          j=j+1
          new_Components[[j]] = C
          C_old = Components[[j]]
          Components[[j]] = C # del
          R = R - C$pred
          
          if (show){
            plot(time,signal,t='l',ylim=c(-2,7),main=paste('calibration',t),bty = "L",col=gray(0.5))
            polygon(c(time[1],time,time[length(time)]),c(0,signal,0),col=gray(0.8),border=NA)
            #lines(time,C_old$pred,col=gray(0.3))
            #draw.decomposition(new_Components,time,pow=j)
            fake_comp = list(C_old,pow=1)
            
            draw.decomposition(Components,time,pow=pow)
            draw.decomposition(fake_comp,time,col='#00000000',col2=1)
            
          }
          
        } 
        
      }
      pow=j
      Components=new_Components
      
      err0 = err
      err  = sum(R^2)
      
      if (show){
        plot(time,signal,t='l',ylim=c(-2,7),main=paste('calibration',t),col=gray(0.5),bty = "L")
        polygon(c(time[1],time,time[length(time)]),c(0,signal,0),col=gray(0.8),border=NA)
        #lines(time,R)
        draw.decomposition(Components,time,pow=pow)
      }
    
      }
  }
  
  if (pow==1){
    Components[[1]] = fit.component(signal, time, NaN, smooth)
  }
  
  if (show){
    plot(time,signal,t='l',ylim=c(-2,7),main='final')
    draw.decomposition(Components,time,pow=pow)
  }  

  Components$pow=pow
  Components
}



## decomposition function, decomposes all singnals in a single frame. Does the pre-processing
##    Input:
## data - data with the decomposed frames
## signalname - name of the field there the signal is contained
## d - kernel variance for the preprocessing (parameter b)
## treshold - treshold parameter

MS.frame.decompose  <- function(data,
                                signalname='Signal',
                                d=0.5,
                                ... ) 
{ 
  frame = data$data_frame
  D = PM.get.grow(frame,signalname=signalname,d=d)
  results=list()
  Substrates = unique(frame$Substrate)
  for (sub in Substrates){
      print (sub)
      signal = D$Growth[ D$Substrate==sub ]
      time = D$Time[ D$Substrate==sub ]
      results[[sub]] = MS.decompose(signal,time,d,...)        
    }
  data$Decomposition = results
  data$data_frame$Growth = D$Growth
  data
}




MS.similarity   <- function (A,B,a=3,b=100,c=30)
{
  if ((A$pow==0) & (B$pow==0)) {result=1}
  if ((A$pow*B$pow==0) & (A$pow+B$pow>0)) {result=0}
  if ((A$pow>0) & (B$pow>0)) {

    if (A$pow>B$pow){
      S = B
      L = A
    } else {
      S = A
      L = B     
    }
    
    M = matrix(0,L$pow,6)
    colnames(M) = c('A_max','A_size','A_mean','B_max','B_size','B_mean')
    for (i in 1:S$pow) {
      M[i,'A_max']  = max(S[[i]]$pred)
      M[i,'A_size'] = sum(S[[i]]$pred)
      M[i,'A_mean'] = sum(S[[i]]$pred * (1:length(S[[i]]$pred)) ) / sum(S[[i]]$pred) /4
    }
    for (i in 1:L$pow) {
      M[i,'B_max']  = max(L[[i]]$pred)
      M[i,'B_size'] = sum(L[[i]]$pred)
      M[i,'B_mean'] = sum(L[[i]]$pred * (1:length(L[[i]]$pred)) ) / sum(L[[i]]$pred) /4
    }
    
    
    library(gtools)
    P = permutations(L$pow, S$pow)
    result = 0
    for (ppp in 1:dim(P)[1]){
      cur=1
      for (i in 1:S$pow) {
        j = i
        k = P[ppp,i]
        
        cur=cur*exp(-(M[j,'A_max']- M[k,'B_max'])^2/a^2/2  ) 
        cur=cur*exp(-(M[j,'A_size']- M[k,'B_size'])^2/b^2/2  )  
        cur=cur*exp(-(M[j,'A_mean']- M[k,'B_mean'])^2/c^2/2  )
        
        if (cur>result)  result =  cur
      }
    }
    result = result[[1]]
  }
  result
}


#####################
### Visualization ###
#####################



## Draws the whole decomposition
##     Input:
## Components - decomposition
## time - time points
## t - type of the visualization. Options are sum, overlap, cumsum
## pow - number of components to be visualized
## col, col2 - colors for the component and its border
## lwd - line width


draw.decomposition = function(Components,time,t='overlap',pow = Components$pow,col=-1,col2=-1,lwd=2) {
  if (pow>0){
    n=length(time)
    R=rep(0,n)
    for (i in 1:pow){
      C = Components[[i]]
      if (col==-1){
        if (C$model=='gauss') {col1 = '#0000FF77'; col2=4}
        if (C$model=='brick') {col1 = '#FF000066'; col2=2}
        if (C$model=='slope') {col1 = '#00AAAA66'; col2='#00AAAA'}
      }else{
        col1=col
      }
      if (col2==-1){
        if (C$model=='gauss') {col2=4}
        if (C$model=='brick') {col2=2}
        if (C$model=='slope') {col2='#00AAAA'}
      }
      if (t=='sum'){
        polygon(c(time, time[n:1]),c(C$pred+R,R[n:1]),col=col1,border=NA)
        lines(time,C$pred+R,col=col2,lwd=lwd)
        R=R+C$pred
      }
      if (t=='overlap'){
        polygon(c(time[1],time,time[length(time)]),c(0,C$pred,0),col=col1,border=NA)
        lines(time,C$pred,col=col2,lwd=lwd)
      }
      if (t=='cumsum'){
        polygon(c(time, time[n:1]),c(cumsum(C$pred)+R,R[n:1]),col=col1,border=NA)
        lines(time,cumsum(C$pred)+R,col=col2,lwd=lwd)
        R=R+cumsum(C$pred)
      }
    }
  }
}



## Draws the decompositions for the whole plate
##     Input:
## data - plate
## cex - font size
## signalname - 
## t - type of the visualization. Options are sum, overlap, cumsum

MS.plot.decomposition <- function(data,
                                  cex=0.5,
                                  signalname='Growth',
                                  t='sum'
)                                  
{
  frame = data$data_frame
  times = unique(frame$Time)
  
  NUMBERS = c('01','02','03','04','05','06','07','08','09','10','11','12')
  for (i in 1:8){
    for (j in 1:12){
      sub = paste(LETTERS[i],NUMBERS[j],sep='')
      subdata= frame[frame$Substrate==sub,]
      D = data$Decomposition[[sub]]
      if (i==j & i==1){
        par(fig=c(0,2/9,  0.02,2/13), new=FALSE)
        par(mar=c(2.0,2.0,0.1,0.1))
        par(mgp=c(1.0,0.5,0))
        par(cex.axis=cex,cex.lab=cex)
        par(xaxs='i',yaxs='i')
      }else{
        par(fig=c(0,1/9,  0,1/13)+c(i/9,i/9,j/13,j/13), new=TRUE)
        par(mar=c(0.1,0.1,0.1,0.1))
      }
      
      if (t=='cumsum'){
        maxval = 400
      }else{
        maxval = 7      
      }     
      plot(c(min(times),max(times)),c(0,maxval),t='n',axes=FALSE,xlab='Time',ylab='Growth')
      
      title(sub, cex.main=0.8, line=-1, col.main='#888888')
      #box(col=gray(1-D$pow/3),lwd=D$pow+0.1)
      if (i==j & i==1){
        axis(1, col='gray')
      }
      if (signalname=='Growth')
        lines(times,subdata[[signalname]],col=1)
      else
        lines(times,subdata[[signalname]]*maxval/400,col=1)
      draw.decomposition(D,times,t=t) 
    }
  }
}



